\subsection{Weighted-Residual Minimization, or Variational, Methods}

In a generalized weighted-residual method, the problem is solved by
taking the inner product of our equation, with a test function:
\begin{equation}
   \innerprod{\linop \uu}{\vv} = \innerprod{\ff}{\vv}.
\end{equation}
The innerproduct used here is defined in the typical way
\begin{equation}
   \innerprod{a}{b} = \int_\Omega a b \ d^3x
\end{equation}
where $\Omega$ is the domain.
The function $\vv$ is known as the test function and it must live in the data
space $\Vspace$, as seen in \reffig{fig:spaces}.  The inner product here is also
in $\Vspace$.
To numerically solve this problem, the solution $\uu$ is expanded in a
set of trial functions to give $\uuh \in \Uspaceh$.
Likewise, the discrete form of $\vv$ is in the test space $\vvh
\in \Vspaceh$.  
The numerical procedure then is to minimize the following integral:
\begin{equation}
         \int \lp \linoph \uuh - \ffh \rp \vvh \dV
\end{equation}
As before, $\linoph \uuh - \ffh$ is the consistency error, or the residual. The
goal is minimize this residual weighted with the test function, $\vvh$, which is
why these methods are called weight-residual minimization methods.  
These
methods include Fourier transforms, where the basis functions are exponentials;
collocation methods, where the test function is a delta function; or finite
element methods, where the test and trial functions are polynomial expansions
within a cell.

\input{figures/wrmethod_spaces}

This equation can be written in the following form:
\begin{equation}
      \bilinear{\uu}{\vv} = \Flinear
\end{equation}
where the bilinear form is defined as $\bilinear{\uu}{\vv}\innerprod{\linop
\uu}{\vv} $ $\bilinear(:;:) : \Uspace \times \Vspace \rightarrow \realspace$ and
the linear form $\Flinear = \innerprod{\linop \uu}{\vv}$.  The linear functional
also defines a duality pairing between $f$ and $v$ such that if $v$ is in space
$\Vspace$, then $f$ is in $\Vdual$.  That is, in reference to the discussion in
Sec.~\ref{basic}, we can write $\Fspace = \Vdual$.  
Taking the inner product with the test function is a projection of the solution
space into real space, and so this method is also a {\em quasi-projection}
method.  

Our spaces defined here are allowed to be more rigorous than the spaces defined
previously in \refsec{basic}.  An version of \reffig{fig:spaces} specifically for the
weighted-residual method is shown in \reffig{fig:wrmethod_spaces}.  The details
of the null and domain are still valid, but are not shown.  The linear and
bilinear forms are not maps between the relevant spaces because both forms map
the spaces into the real number space, but it is still useful to understand the
space.  These spaces are Sobolev spaces as seen in
\reffig{fig:overview_spaces} because we are interested in the continuity of test
and trial functions.

A bilinear operator is said to be {\em coercive}, or V-elliptic, if it satisfies 
\begin{equation}
         \ccoerce \norm{\uu}^2 \leq \bilinear{\uu}{\uu},
\end{equation}
and is continuous if it satisfies
\begin{equation}
       \bilinear{\uu}{\uu} \leq \ccont \norm{u}\norm{v}
\end{equation}
The Lax-Milgram theorem states that if a bilinear operator is continuous and
coercive, then there is a unique solution to the variational problem:
$\bilinear{\uu}{\vv} = \Flinear$, and a unique solution to the discrete
equivalents.  The coercivity condition can be used to show stability of the
discrete equations:
\begin{equation}
  \norm{\uuh}^2 \leq \bilinear{\uuh}{\uuh} = \innerprod{\ff}{\uuh} \leq \norm{\ff}\norm{\uuh}.
\end{equation}
The formal proof~\cite{AtkinsonHan} demonstrates the natural relationship
between consistency ($\ccons$) and continuity ($\ccont$), and stability
($\cstab$) and coercitivity ($\ccoerce$).  

Formally, the Lax-Milgram theorem expressed here only applies to true
variational problems where the solution of the above can be cast as a
minimization problem of an associated energy functional that is convex; i.e.,
for symmetric operators, $\linop$, like the Poisson equation.  For more general
problems, the coercivity condition can be relaxed to the \emph{inf-sup}
condition, also known as the Ladyzhenskaya–Babu\u ska–Brezzi (LBB)
condition~\cite{AtkinsonHan}.  In this case, the problem isn't a convex
optimization problem, but rather a saddle point problem.

Before studing the general case, we discuss the variational approach in more
detail.  In the variational approach, the linear equation is $\linop \uu = \ff$
is related to the quadratic (energy) functional:
\begin{equation}
      E(\vv; \ff) = \half \lp \linop \vv, \vv \rp -\lp \ff, \vv \rp
      \label{EnergyFunctional}
\end{equation}
Using the Calculus of Variations, it can be shown that if $\uu = \vv$ is the
minimization of $E$, then $\uu$ satisfies $\linop \uu = \ff$.

It is worth considering the easiest equation that variational
approaches do well:  the Poisson equation.
The Poisson equation given by:
\begin{equation}
      \gradsq \uu = \ff
      \label{Poisson}
\end{equation}
solved over the domain $\Omega$ with $\uu = 0$ on the boundary, $\partial
\Omega$.
Integrating by parts, and explicitly showing the integration implied by the
inner product, the $E$ functional is given by 
\begin{equation}
      E(\uu; \ff) = \frac{1}{2} \int_\Omega \mid \grad \uu \mid^2 - \ff d\Omega.
      \label{PoissonEnergy}
\end{equation}
This is written in a nice form that shows that the functional is convex; i.e.,
it is like finding the minimum of a parabola~\footnote{This is covered in many
textbooks, but Strang and Fix~\cite{StrangFix} have a particularly lucid
explanation.}.  Numerically minimizing a convex functional is known as the Ritz
method, or (inaccurately) as the Rayleigh-Ritz method. 

It can be shown~\cite{BochevICM:2006,bochevGunzburger2009} that solving variational
problems that have convex, quadratic functionals have a number of attractive
features:
\begin{itemize}
      \item All variables can be treated with a single type of finite element,
            such as linear $C^0$ elements,
      \item General regions and boundary conditions are easy to grid and solve
            for,
      \item Resulting linear system is symmetric,
      \item Resulting linear system is positive definite,
      \item Conformal finite elements; where the approximating space is a
            sub-space of the infinite dimensional, continuous space; guarantees
            stability and optimal accuracy.
\end{itemize}
For example, $C^0$ finite elements works well in solving the Poisson
equation.  

The success of finite element methods lead to their use in problems that do not
have a convex functional.  In these cases, Items 1 and 2 are general properties
of finite elements that carry over to the solution of any PDE, but the other
attractive features are lost.   Going from a convex problem to a saddle point
problem introduces many problems, and many approaches have been attempted to
overcome them.  We discuss these different approaches next.


\subsection{Approaches to Coercivity}

Consider the Stokes problem:  its operator is non-symmetric, and thus its
resultant linear system is neither symmetric nor positive definite.  In
addition, because of the topology of the operator with the divergence constraint
on the velocity has difficulty in creating a conformal discretization.
One method for overcoming this problem is to use Lagrange multipliers to create
a variational problem.  However, the resultant variation requires solving not a
minimization problem, but a saddle-point problem.  Although it is a true
variational problem, it still requires more care.

Yet another approach presents itself:  Discretize the least-squares error.  This
naturally presents a new energy functional:
\begin{equation}
      E(\uu; \ff) = \lip \linop \uu - \ff, \linop \uu - \ff \rip
      \label{LSEnergy}
\end{equation}
Because of the symmetry of the problem, this functional is convex and the
solution to it therefore places it into the Ritz
setting~\cite{jiang1998least,bochevGunzburger2009}.  The equivalent variational
equation is $\linop^{-1} \linop \uu = \linop^{-1} \ff$.   Using standard finite
element techniques using this functional is known as the Least-Squares Finite
Elements Method (LSFEM), as shown in \reffig{fig:discretizations}.

This method offers many attractive qualities and recovers many of the desirable
features above.  However, there are some obvious drawbacks.  The equivalent
matrix equation arising from $\linop^{-1} \linop$ will be symmetric, but the
condition number of the resultant matrices will be squared.  To overcome this,
one can rewrite the equations into a series of first-order equations.  The
resultant method is known as First-Order System Least-Squares
(FOSLS)~\cite{berndt1997local}.  There are many attractive qualities to this
method and Ref.~\cite{bochevGunzburger2009} provides an excellent review;
however, as indicated by the condition number issue, considerable analysis and
work is required to make it an effective scheme in general.

To consider other issues with this method, we consider the general
transport equation:
\begin{equation}
      \dt{\uu} + \dive{\vF(\uu)} = S
\end{equation}
where $\vF$ is the linear or nonlinear flux, and $S$ is the source.
It can be shown, that the fluxes are convex with respect to $\uu$; i.e., 
$\vF^{\prime\prime}(\uu) > 0$; then discontinuities can arise over
time~\cite{hesthaven2017numerical}.  For example, the advection operator can
lead to shock formation, or the development of contact discontinuities.
In addition to these discontinuities, a given problem might also have material
discontinuities, and these again, may be time dependent.
%\todo{Include proof}

%\todo{Rewrite this}
Much of the work in making LSFEM effective has focused on using standard $C^{0}$
finite elements, including high-order versions thereof.  But given this
discontinuities, it is worth considering discontinuous versions there-of.  There
are two closely-related versions:  Discrete
Least-Squares~\cite{keith2017discrete} (DLS) and Discontinuous Petrov-Galerkin
(DPG)~\cite{Demkowicz:2010co}.  Because DPG is more mature and better
understood, we will focus on this method.

In this method, the discretized method to be solved is:
\begin{equation}
      \lip \linop \uuh - \ffh, \vvh \rip = 0,
\end{equation}
where $\vvh \approx \linop \uuh - \ffh$ is a numerically-calculated optimal
test function using discontinuous elements.  More formally, DPG uses
dual norms in an explicit element-wise inversion of the Riesz operator made
possible by the use of discontinuous elements~\cite{BuiThanh:2013in,keith2017discrete}.  In
this way, DPG has the advantages of automatically satisfying the $inf-sup$
stability condition optimally, while allowing the discrete trial and test
spaces to be different.~\footnote{There are multiple approaches to developing
DPG and this is more of a heuristic guide to emphasize the unity of approaches.}
That is, the advantages of DPG over standard LSFEM elements is to 1) use
discontinuous elements that more naturally fit problems that have
discontinuities,  2) allow a numerically tractable problem by allowing
element-wise computations for computing the optimal test space, and 3) providing
a wider class of approaches to convergence by allowing the trial and test spaces
to be different.  Ideally, it enables these advantages while maintaining automatic
stability and optimal convergence; however, the calculation of the optimal test
functions on an element-by-element basis means that the formal optimal
convergence properties are lost at the expense of better computational
performance.  For more on the relationship of LSFEM and
DPG, Ref.~\cite{keith2017discrete} provides a useful reference.

In theory therefore, DPG seems like the optimal discretization method.  In
practice, the computational efficiency of DPG may not be optimal.  For nonlinear
time-dependent problems, calculating the test function at every time step might
be computationally costly.  Much of the DPG literature concerns optimizing DPG
for various problems by analytically exploiting the mathematical structure of
equations (e.g., ~\cite{BuiThanh:2013in} and references therein).  For problems
with nonlinear materials, or complicated sub-grid models, these analytic
reduction techniques might not be optimal, and thus the method expensive.
One simple method for approximating DPG methods would be to develop an {\em a
posteriori} error estimator for when the test function needs to be
re-calculated.  This method has not been explored sufficiently at the present
time.

\input{figures/discretizations}

It is of interest to consider approximations to DPG.  The most popular method is
the Discontinuous Galerkin (DG)
method~\cite{cockburnShu1989,cockburn2012discontinuous}.  This method was not
developed as an approximation to DPG, but rather as a method of bring some
advantages of finite volume methods to the finite-element setting.  However, as
shown in Ref.~\cite{Buithanh:2011vo}, the DG method can be seen as an
approximate form for the DPG method.  The DPG method also has the advantages of
explicitly specifying the jump conditions across elements; whereas, for the DG
methods, the jump conditions are specified via an external method; e.g., by
solving the Riemann problem as in finite volume Godunov methods.  For example,
for the Poisson solve problem, while the standard Galerkin method gives a simple
method as discussed above, the DG method has multiple methods not all of which
have optimal convergent properties~\cite{arnold2000}.  In the language of
finite-volumes, the jump conditions come from {\em reconstructed} fluxes,
rather than {\em evaluated} fluxes as would be the case in a
fully-evaluated weighted-residual method.

This problem is addressed in the hybridized discontinuous Galerkin (HDG) of
Bui-Thanh~\cite{BuiThanh:2015bw}.  Although it can be viewed as part of the DG
method, the fact that it is ``parameter-free'', like the DPG method, makes it
attractive as a separate method, especially for the cases where there is not a
rich literature of how to form the proper jump conditions, as there are in CFD
methods.  Thus, DPG, HDG, and DG are all closely related methods with increasing
approximations, but all having the skeleton discretization to develop
discontinuities across the cell boundaries.

These additional degrees of freedom, although they allow for the flexibility
needed to capture discontinuities, they come at a considerable computational
cost.  For example, assuming about seven linear tetrahedral elements per node,
the \textbf{DG system involves approximately 28 times the number of unknowns of the
corresponding continuous Galerkin system}~\cite{hughes2000}.  If there was a way
of transmitting the information from the skeleton mesh to the continuous mesh.
This idea is explored in
References~\cite{Bochev:2005fv,HughesScovazziBochev2006}.  Although they
classify it as a as a ``DG method with the structure of a CG method'', the
``interscale transfer operator'' that they develop results in a method that
looks like stabilized methods developed under prior techniques.  Thus we
classify it as a ``stabilized FE'' technique, which includes the well-known
Stream-Upwind Petrov-Galerkin (SUPG) method~\cite{brooks1982supg} and
Variational Multi-Scale (VMS)
method~\cite{hughes1995multiscale,Hughes:tr,Codina:2018ds}.  It might be
possible to numerically develop these techniques to allow a truly new stabilized
method that allows for a provably stable method with $C^0$ elements but at
present, it is not clear that it represents an advance on the traditional
stabilized methods.  

Stabilized finite-element methods are widely used industrially as at, for
example, Boeing for designing aircraft.  The biggest problem with them is that they have a limited
regime of validity, and for developing new algorithms for new PDEs, considerable
work must be performed in 1) analytically deriving the appropriate stabilizing
operator, and 2) numerically verifying that the range of its validity is large
enough to be useful.  

Thus, within the method of weighted residual methods, we can give an heuristic
hierarchy of methods (not complete!) as shown in \reffig{fig:discretizations}.
The mathematical rigor decreases as one moves down the figure, but so does the
computational complexity.  It should be clear that if stabilized FE methods work
for your problem, then that should be the preferred method.  But given the
uncertainties in determining the range of stabilization {\em a priori}, as well
as issues such as sensitivity to mesh quality, it should be clear that there is
not a single method that is best for all problems.  Thus, we are forced to
conclude:
\begin{quote}
      The best computational framework for the Air Force must allow flexibility
      in choosing the numerical discretization scheme.
\end{quote}
This general statement still belies all of the knowledge that has been gained in
computational science; indeed, there is enough experience to offer some general
heuristics and that is discussed next.
\begin{table}
      \begin{tabular}{ll}
            \bf{Operator} & 
            $\linop{\uu}=\ff$  
       \\
            \bf{bilinear form:} & 
            $\bilinear{\uu}{\vv}=\load$  
      \end{tabular}
      \begin{tabular}{ll}
            \bf{Galerkin FE:} & 
            % I have no idea why this : causes problems
            ${\bilinearop}$: $\Uspace \times \Uspace \rightarrow{\mathbb R}$
       \\
            \bf{LSFEM:} & 
            % I have no idea why this : causes problems
            ${\bilinearop}$: $\Vspace \times \Vspace \rightarrow{\mathbb R}$
       \\
            \bf{DPG} & 
            % I have no idea why this : causes problems
            ${\bilinearop}$: $\Vspace \times \Vdual \rightarrow{\mathbb R}$
      \end{tabular}
\end{table}
