#
# Version:	$Id: Makefile 1195 2016-11-15 00:46:17Z jking $
#
##########

# These are spell and grammar checked
INPUTFILES = intro.tex formalisms.tex implementation.tex discussion.tex functional_analysis.tex unified.tex wrmethod.tex
# These aren't spellchecked
OTHERFILES = definitions.tex

MAIN	= discretization
BIBFILES = appmath.bib
FILES = $(MAIN).tex $(INPUTFILES) $(OTHERFILES) $(BIBFILES) # Dependencies

.SUFFIXES: .pdf .tex

SHELL = /bin/bash
ASPELL = `which aspell`
JAVATEXTIDOTE =  /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -jar ~/config/lib/textidote.jar
LATEX=pdflatex -shell-escape

all: $(MAIN).pdf merge

$(MAIN).pdf:  $(FILES)
	$(LATEX) $(MAIN)
	$(LATEX) $(MAIN)
	bibtex $(MAIN)
	$(LATEX) $(MAIN)
	/bin/cp -f $(MAIN).pdf DraftPDF
	@make findref
	@make findlabel

fast: $(FILES)
	$(LATEX) $(MAIN)
	/bin/cp -f $(MAIN).pdf DraftPDF

merge: exportbib $(FILES)
	@cp $(MAIN).tex temp.tex
	@for file in $(INPUTFILES) $(OTHERFILES); do \
	  filebase=`echo $$file | sed "s/.tex//"`; \
	  echo $$file ; \
	  cat temp.tex | sed "s/\\\input{$$filebase}/replaceline/" | awk '$$1=="replaceline" {{system("cat '$$filebase.tex'")} {next}} {print}' > temp2.tex; \
	  mv temp2.tex temp.tex; \
	done
	@cat temp.tex | sed 's|\\bibliography{.*|\\bibliography{merged}|'  > temp2.tex && mv temp2.tex temp.tex
	@cat temp.tex | sed 's|figures\/||' | sed 's|.png||' > DraftTex/$(MAIN)_merged.tex
	@cp $(MAIN).bbl  DraftTex/$(MAIN)_merged.bbl
	@rm -f temp.tex

# Create a bib file with just the cited entries
exportbib:
	@bibexport -o DraftTex/merged.bib $(MAIN).aux

grammarcheck: 
	@echo Grammar checking TeX files. 
	for file in $(INPUTFILES); \
	do  echo ==== $$file;\
	    $(JAVATEXTIDOTE) $$file;\
	done

spellcheck: jargon.spl
	@echo Spell checking TeX files. 
	@if [ -x $(ASPELL) ]; then\
	  for file in $(INPUTFILES); \
	  do  echo ==== $$file;\
	      $(ASPELL) --lang=en --mode=tex --ignore-case --add-extra-dicts ./jargon.spl check $$file;\
	  done; else\
	  echo ASPELL not found.;\
	fi

jargon.spl: jargon.txt
	@echo Updating jargon dictionary.
	aspell --lang=en create master ./jargon.spl < jargon.txt

findref: 
	@echo '---------------------------------------------------------'
	@echo Finding undefined references
	@for uref in `grep undefined $(MAIN).log | grep -v There | sed 's/.*\`//g' | sed "s/'.*//g"`; \
	 do  \
		echo "--> $$uref ";\
		grep -n "[rc][ei].*$$uref" *.tex | grep -v '.tex:[0-9]*:%' | grep -v _merged |sed 's/:/ -- Line /' | sed 's/:/:  /';\
		echo ;\
	  done
#	@grep undefined $(MAIN).log | grep -v 'There were'

findlabel:
	@echo '---------------------------------------------------------'
	@echo Finding multiply defined labels:
	@file=$(<) && for mlab in `grep 'multiply defined' $(MAIN).log | grep -v _merged | sed 's/.*\`//g' | sed "s/'.*//g"`; \
	 do  \
		echo "--> $$mlab ";\
		grep -n "label.*$$mlab" *.tex | grep -v _merged;\
		echo ;\
	  done

remake: clean all

clean:
	rm -rf core
	rm -f *~ *.prev *.log *.end *.aux 
	rm -f *.bbl *.blg  *.toc *.ps *.pdf
	rm -f $(MAIN).out

realclean: clean
	rm -f *.pdf

.PHONY: full findlabel clean realclean merge exportbib remake findref
