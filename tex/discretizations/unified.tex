\subsection{Basic concepts in computational modeling}
\label{basic}

Computational science has long been termed the third branch of
science~\cite{pool1992third}.  Historically, the core of this branch is solving
mathematical models that are typically partial differential equations (PDEs).
The general procedure is to transform the continuous PDE into a discretized form
that is amenable to computer solution.  Although this discretization process can
be traced to World War II or earlier, this area remains an active area of
research because 1) new problems arise that need new solutions, 2) exploiting
the full power of modern computer hardware requires changing algorithms, and 3)
numerical research itself is leading to improvements in algorithms.  The long
history had lead to a variety of approaches that can be overwhelming, but
fundamental principals exist to guide any computational scientist seeking to
numerically solve a challenging problem.

Currently, there are three dominant paradigms for discretizing PDEs: finite
differences, finite volumes, and finite elements (or more generally, {\emph
weighted residual methods}.  Finite differences are the oldest of the three and
are based on Taylor expansions about a given point.  They can be difficult to
implement for complex geometries, although work on cut-cell methods, which
enable interfacing to arbitrary boundaries, are continuing.  Finite volume
methods where integral fluxes are approximated via quadrature.
Weighted-residual methods, also known as variational methods~\footnote{These
methods are often called variational methods even when they are not strictly
formulated as a variational problem}, use expansions of the variables in test
functions, integrating the result over trial functions, and then minimizing the
weighted residual.

For all of these methods, the process for achieving a numerical solution for a
given problem is the same.  It is:
\begin{center}
\begin{tabular}[t]{lL}
      1. Specify equations:      & \linop \uu = \ff
                                                                  \\ 
      2. Discretize equation:   & \linoph \uuh = \ffh
                                                                  \\ 
      3. Solve equation:        &  \uuh =\linoph^{-1} \ffh
\end{tabular}
\end{center}
where the $h$ superscript denotes the discretized form of the operators,
solutions, and the size of the discretized element or cell, $h$.  
Here, we use $\linop$ to denote the equations because most of the analysis will
use linear operators.  Nonlinear solutions generally involve a linearization
about either an iterate or a time step, so the linear analysis 
forms the basis for further analysis.

It is clear that the goal of numerically solving an equation is that one
needs $\uuh$ to converge to the original mathematical solution $\uu$;
i.e., $\norm{\uuh - \uu} \rightarrow 0$ as $h \rightarrow
0$~\footnote{Details on how to take this difference and the norm can be
extremely important, and not always intuitive~\cite{arnold2015}}.
Convergence is almost impossible to prove directly.  Fortunately, we
have the Lax Equivalence Theorem (and generalizations
thereof)~\footnote{The original Lax Equivalence theorem was derived only
for finite-difference methods.}: 
%
\begin{center}
      \textsc{Consistency} + \textsc{Stability} = \textsc{Convergence}
\end{center}
Although convergence is hard to prove, consistency and stability are
able to be proven, and we outline the general steps.

The first step is to insure that the discretized equation is a good
approximation to the original equation, or consistent.  The methods for proving
this vary depending on the method, but is perhaps easiest in finite differences
where the development of the method leads to the known truncation error.
Showing that  error goes to zero as $h$ goes to zero~\cite{Strikwerda}, proves
consistency.  

More formally and more generally, consistency can be defined as
\begin{equation}
      \label{consistency1}
      \norm{\linop \uu - \linoph \uuh} \rightarrow 0 
\end{equation}
as $h \rightarrow 0$ for the appropriately defined
norm.  This is generally intuitive, but these differences can be difficult to
define rigorously.  To understand this, it is helpful to consider the spaces
that the solution ($\uu$), and the data ($\ff$), or load, live on.  In
\reffig{fig:spaces}, the relevant spaces are defined.  The solution space is
denoted as $\Uspace$, and the data space is denoted by $\Fspace$.  The linear
operator, $\linop$, is a map from the solution space to the data space.  The
discretization of the solution is given by an operator, $\repU$, such that $\uuh
= \repU \uu$.  This $\uuh$ is called the {\em representative} of
$\uu$~\cite{arnold2015}.  As seen in \reffig{fig:spaces},
$\repU$ is a map from the
continuous space, $\Uspace$ onto the mesh space $\Uspaceh$.  Likewise for the
operator, $\repV$ for the continuous data space and discrete data space.  The
discrete operator, $\linoph$, maps from $\Uspace$ onto the mesh space
$\Uspaceh$.  

Consistency error arises because the two paths from the solution space to the
discrete data space are not commutative.  Lets consider the down-right path.
A solution $\uu$ is discretized to give its representative, $\uuh = \repU \uu$.
The discrete form of the operator is applied to give $\linoph \uuh = \linoph
\repU \uu$.  The right-side of our equation likewise has a representative $\ffh
= \repV \ff = \repV \linop \uu$. This is the right-down path on our diagram. 
Our consistency error therefore is:
\begin{equation}
      \norm{\linoph \uuh - \ffh} =  \norm{\linoph \repU \uu - \repV \linop \uu}.
\end{equation}
This difference is more rigorous than that given by \refeq{consistency1} as it
is in the same space.  A scheme is said to be consistent if there exists a
constant independent of $h$ such that 
\begin{equation}
      \norm{\linoph \repU \uu - \repV \linop \uu} \leq \cconsh h^r,
  \label{ccons_inequality}
\end{equation}
where $r$ is the {\em order} of the discretization.  The denoting of the constant
with the $h$ subscript is because it represents a measure of the continuity of
both the continuous operator {\em and} the discrete operators.

\input{figures/discretization_spaces}

The second step in discretization is to insure that the solution is stable, or
colloquially, does ``not blow up''.  More formally, imagine that the
discrete data is perturbed, $\tilde{\ffh} = \ffh + \epsilon_h$
resulting in a perturbation to the discrete solution
$\tilde{\uuh}=\linoph^{-1} \tilde{\ffh}$.  The amount of perturbation in
the solution normalized to the initial perturbation is given
by~\cite{arnold2015}:
\begin{equation}
      \frac{\text{solution perturbation}}{\text{data perturbation}}
            = \frac{\norm{\tilde{\uuh}-\uuh}_h}{\norm{\tilde{\ffh}-\ffh}_h}
            = \frac{\norm{\linoph^{-1}\epsilon_h}_h}{\norm{\epsilon_h}_h}
\end{equation}
The norms given above are with respect to their spaces, $\Uspaceh$ and
$\Fspaceh$.

We can more quantitatively measure the stability through a {\em stability
constant, $\cstabh$} that is the maximum value of this ratio, or more formally
the \emph{sup}, for any perturbation in the data:
\begin{equation}
      \cstabh = \sup_{0 \ne \epsilon_h in \Fspaceh}
            \frac{\norm{\linoph^{-1}\epsilon_h}_h}{\norm{\epsilon_h}_h}
            = \norm{\linoph^{-1}}_\linop(\Uspaceh,\Fspaceh).
  \label{stabconst}
\end{equation}
Stability is a statement that this constant be finite; i.e., that
$\cstabh \leq \cstab$ for all $h$
In finite-difference time-domain methods, a Von Neumann analysis
starting with $u(t+\delta t)= u(t) e^{i \lambda \delta t}$ requires that
$\lambda \leq 1$ for stability; i.e., the solution does not blow up.  
There is a direct correlation in this analysis to the amplification
factor and the stability constant.  In the next section, we will explore
the stability factor in terms of weighted-residual methods.

In summary, the general procedure for solving a computational problem has a
number of steps with conditions that need to be satisfied at each step:
\begin{center}
\begin{tabular}[t]{lLll}
       Specify equations:      & \linop \uu = \ff          
     & Discretizable:  $\linop \rightarrow \linoph$  
     & Form of operator is optimal for $\linoph$
                                                                  \\ 
     & \bf{Goal}:       
     & Convergence  $\norm{\uuh - \uu} \rightarrow 0$ 
     & 
                                                                  \\ 
   Discretize equation:   & \linoph \uuh = \ffh 
                          & Consistency: $\norm{\linop \uu - \linoph \uuh}
                          \rightarrow 0$   \ \ \ 
     & Error goes to zero as $h \rightarrow 0$
                                                                  \\ 
   Solve equation:        &  \uuh =\linoph^{-1} \ffh  \ \ \ 
                          & Stability  $\norm{\linoph^{-1}} < C$ 
     & Solution doesn't ``blow up''
\end{tabular}
\end{center}
A large number of mathematical techniques have been developed for each step,
especially with variational methods where functional analysis can be applied.

A simplified proof of the Lax Equivalence Theorem is then
\begin{equation}
      \uu - \uuh = \linoph^{^-1} \lb \linoph (\uu - \uuh)\rb
                 = \linoph^{^-1} \lb \linoph \uu - \linoph \uuh\rb 
                 = \linoph^{^-1} \lb \linoph \uu - \ffh\rb.
\end{equation}
Using the more rigorous definition of the consistency error, and 
and using the inequality given in \refeq{stabconst} gives
\begin{eqaligned}
      \norm{\uu - \uuh} &= \norm{\linoph^{^-1}} \lb \norm{\linoph \repU \uu - \repV \linop \uu} \rb
      \\
                 &\leq \cstabh \cconsh h^r,
\end{eqaligned}

This is the Lax Equivalence Theorem, sometimes called the fundamental theorem of
numerical analysis: {\em a discretization scheme that is consistent and stable
is convergent.}

In terms of our spaces shown in \reffig{fig:spaces}, the difference is taken in
$\Fspaceh$, and that represents the consistency error, and $\linoph^{-1}$ is
used to map this difference back to $\Uspaceh$.  The convergence difference $\uu
- \uuh$ then should be viewed as being taken in $\Uspaceh$.  

The derivation outlined here then is somewhat heuristic because we have not
rigorously defined the norms and spaces, but it applies to all discretization
schemes:  finite differences, weight-residual minimization, finite volumes.
More specifically, if $\Uspaceh \subset \Uspace$ and $\Fspaceh \subset \Fspace$,
the discretization is said to be {\em conformal}.  For conformal discretization
schemes, rigorously defining the differences is straightforward.  For
non-conformal schemes, more care may be needed.  We also note another important
point in numerical solution.  If $\Uspaceh$ has no subset lying in the null
space of $\Uspace$; i.e., $\Uspaceh \not\subset \nulls{\Uspace}$; then it is
said to be {\em mimetic}.  Any discretization scheme can be sensitive to whether
it is conformal or mimetic depending on the problem; this includes not only
$\linop$, but the $\ff$ and the specified boundary conditions.  Two example of
mimetic methods are the Yee mesh in finite differences or N\`edelic elements in
finite elements for preserving the $div-curl$ properties in electromagnetics.

We finish this discussion with some generalized comments about problem
difficulty.  The mesh-independent constants, $\cconsh \cstabh$ have equivalents
in the continous linear operator itself, $\ccons \cstab$.  The condition number
of the problem is given by 
\begin{equation}
      \kappa(\linop) = \cstab \ccons.
\end{equation}
For elliptic equations where a matrix is formed from the discrete
equations, the discrete form of this definition of this condition number
matches with the standard definition of condition
number~\cite{Kirby:2010db}.  However, there is a contribution to the
condition number from the form of the linear operator itself:  a hard
elliptic problem will always lead to hard-to-invert matrices regardless
of the form of discretization.  From this discussion, we can state that
another form of the Lax Equivalence theorem: a convergent solution
requires a finite discrete condition number, and that satisfying this
will be harder for linear operators that have a large condition number.

From this view, one can guess problems that will lead to difficulties.  The
consistency-error constant, $\ccons$, is related to the continuity of the
mapping from $\Uspace$ to $\Fspace$.  If $\Uspace$ and $\Fspace$ are
approximately the ``same shape'', the problem is easier.  Operators, or
mappings, that distort this shape will lead to harder problems.  Aa
non-symmetric operator will lead to a difference in ``shape'' between the two
spaces, and affect the continuity accordingly.  A famous example is the the
Stokes' problem.  Finite elements and central finite differences both have
difficulties in solving this problem.

Symmmetric operators can also be difficult.  Consider a two-dimensional,
steady-state, heat-conduction problem given by the Poisson equation with a large
heat conduction coefficient in the $y$-direction and a small coefficient in the
$x$-direction.  Given a two-dimensional symmetric loading, $\ff(x,y)$, the
solution will become 1-dimensional, $\uu(x)$, in the limit of infinite
heat-conduction in the $y$-direction.  Although this is a  symmetric-operator,
this still leads to a distorted mapping between $\Uspace$ and $\Fspace$:  a
continuity ball in $\Fspace$ becomes an ellipsoid in $\Uspace$.  This will be a
hard problem regardless of discretization scheme.  

Considerations of the other constant provides intuition as well. $\cstabh$ is
proportional to the inverse of the discretized operator.  When solutions blow
up, it is often the grid modes (such as cell-to-cell oscillations) that grow;
that is the inverse of $\cstabh$ becomes very small.  The condition number in
matrix theory is related to ratio of the largest eigenvalue to the smallest, and
this holds here.  $\ccons$ is related to the smoothness of thep problem, or the
largest eigenvalue, and $\cstabh$ is related to the tendency of the problem to
give rise to small eigenvalues.

To understand how to {\em optimally} address hard problems, it is useful to
focus on weight-residual minimization methods.  The advantage of these methods
is that they are more amenable to bringing the full arsenal of functional
analysis techniques to understand the problems and we discuss them next.
