vim  \
    discretization.tex \
    intro.tex \
    formalisms.tex unified.tex wrmethod.tex \
    implementation.tex \
    discussion.tex definitions.tex \
    figures/discretization_spaces.tex \
    figures/wrmethod_spaces.tex \
    figures/discretizations.tex \
    figures/overview_spaces.tex figures/spaces.tex
