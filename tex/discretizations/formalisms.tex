\section{Considerations for an discretization framework}
\label{sec:formalisms}

\input{unified}
\input{wrmethod}

\subsection{The Conservation of Ugliness and the Role of Mathematicians}

Often in determining a numerical scheme, a single property is emphasized.  For
example, in finite volumes, conservation of energy is often the over-riding
determination of the quality of the system.  However, for a given dynamical
system, other invariants may be as important.  For turbulence, conservation
of enstrophy is known to be important for the energy cascade.  For
magnetohydrodynamics, helicity is an important conservation for laboratory
experiments.  For some problems, conservation of the null space is important;
e.g., that a divergence-free quantitatively be exactly divergence-free.  Problems
that preserve a null-space are said to be {\em mimetic}.

An over-emphasis on a given test and trial space is common if realistic terms
that are ultimately required is ignored.  Using look-up tables for material
properties, or nonlinear sub-grid models like LES closures, can complicate any
discretization method further complicating the picture here.  The basic issue is
that while the discretization method without these terms work well on that
problems trial and test spaces, these problematic terms may expand the trial and
test spaces for which the discretization method does not work well.  Because
these terms are often ``black boxes'', traditional numerical analysis may not be
adequate.

Over time, various heuristics have grown up around attempting various problems
with different methods.  A brief example of these heuristics are presented here
(without attribution):
\begin{itemize}
      \item Anisotropic methods favor high-order discretization schemes,
      \item Multiple conservation requirements favor high-order schemes,
      \item Discontinuous problems favor low-order discretization schemes,
      \item Stochastic sub-grid models (e.g., PIC, reactions, etc.) tend to
            favor low-order discretization schemes,
      \item Modern architectures with hierarchical memory tend to favor high-order
            schemes,
      \item Particle-in-cell methods (particle advance methods over long time
            scales) favor mimetic discretizations,
      \item Mimetic discretizations tend to be more sensitive to mesh quality,
      \item Complex geometries favor unstructured meshes, but then the
            discretization can become sensitive to mesh quality.
      \item Complex geometries and complex discretization schemes can lead to
            long development times,
      \item Large radius of convergence are useful so that non-savvy users will be
            able to get approximate answers easily, but tend to be inaccurate.
\end{itemize}
Experienced computationalists likely have their own list of heuristics for their
given domain.

When surveying the numerical schemes for only magnetohydrodynamics, Dr. Dalton
Schnack coined the ``numerical law'' {\em Conservation of Ugliness} that
can be stated as {\em any discretization scheme that excels at one
numerical property will be bad at another numerical property.}  While
thought-provoking and containing some truth, it is clear that some numerical
schemes have become dominant workhorses within their application area.  
Of course, this specificity of discretization scheme for solving a specific
problem belies the idea of a general framework devoted to a single numerical
scheme.

Development of workhorse applications has clearly involved the work of applied
mathematicians.  In addition to the development of discretization algorithms
surveyed here, development of new linear algebra solvers has enabled new
algorithms.  For example, many of the ``hybridization'' techniques (putting the
forms into first-order ultra-weak form) for variational problems result in
non-symmetric matrices.  The development of GMRES and multi-grid as a robust
workhorse for these matrices has enabled these methods to increase in
usefulness.  This is not to mitigate the importance of practical applications on
real-world problems, with realistic meshes, sub-grid models, and other points of
failure, but without understanding the generalized Lax-Milgram Equivalence
theorem and it's full implications, a code developer is left through
{\em ad hoc} band-aids forever twiddling to try and overcome fundamental
limitations.
